import { readdirSync } from "fs";

const getAdditionalEnumTypes = (directoryPath: string) =>
  readdirSync(directoryPath).filter(
    (file: string) => !file.startsWith(".") && !file.endsWith(".json"),
  );

export default {
  parserPreset: "conventional-changelog-conventionalcommits",
  plugins: [require("./verify-table.ts")],
  rules: {
    "table-info-required": [2, "always"],
    "type-enum": [2, "always", ["fix", "feat"]],
    "type-empty": [2, "never"],
    "scope-enum": [
      2,
      "always",
      ["ci", "commitlint", "docs", ...getAdditionalEnumTypes("./config")],
    ],
  },
};
