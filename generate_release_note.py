import os
import re

#def is_valid_url(url: str)->bool:
#    return re.match(r"^((http|https)://)[-a-zA-Z0-9@:%._\\+~#?&//=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%._\\+~#?&//=]*)$", url)

def get_airtable_link(airtable_link):
    if airtable_link is not None and len(airtable_link):
        return f"## Airtable\n\n[{airtable_link}]({airtable_link})"
    else:
        return ""


def format_ticket_link(gitlab_ticket: str):
    chiffres = re.findall(r"\d+", gitlab_ticket)
    return f"[{gitlab_ticket}](https://gitlab.com/M6Distribution/datalake/-/issues/{chiffres[0]})"


def get_gitlab_ticket(title):
    if title is None:
        print("No title found for Merge Request.")
        exit(1)
    tickets = re.findall(r"workstream#\d+", title)
    if len(tickets):
        tickets= ",".join(map(format_ticket_link, tickets))
        return f"## Related Gitlab Tickets\n\n{tickets}"
    else:
        print(f"No ticket found in merge request title: {title}")
        exit(1)


if __name__ == "__main__":
    airtable_link = os.environ.get("AIRTABLE_LINK")
    ci_commit_title  = os.environ.get("CI_COMMIT_MESSAGE")

    print(get_gitlab_ticket(ci_commit_title))
    print(get_airtable_link(airtable_link))
