// getting patterns from string message
const getTableName = (inputString: string): string[] =>
  getDatabase(inputString)
    .map((match) => match.slice(1, -1))
    .join("")
    .split(",")
    .map((name) => name.split(".")[1])
    .filter((value) => typeof value === "string" && value !== "");

const getDatabase = (inputString: string): string[] =>
  inputString.match(/\((.*?)\)/g) || [""];

const getTicket = (inputString: string): string[] =>
  inputString.match(/workstream#\d+$/g) || [];

const isTicketGiven = (inputString: string): boolean => {
  return Boolean(getTicket(inputString).length);
};

const isTableNameGiven = (inputString: string): boolean =>
  Boolean(getTableName(inputString).length);

const isDatabaseGiven = (inputString: string): boolean =>
  Boolean(getDatabase(inputString).length);

const isCommitOk = (inputString: string): [boolean, string] => {
  const hasTableChangement = inputString.includes("table");
  const hasDatabase = isDatabaseGiven(inputString);
  const hasTableName = isTableNameGiven(inputString);
  const hasTicket = isTicketGiven(inputString);

  /*
    A good commit message is either :
     * a commit without table keyword
     * a commit with table keyword and all the required information such as database, table name and ticket
    */

  if (!hasTicket) {
    return [false, "gitlab ticket is missing"];
  }
  if (!hasTableChangement) {
    return [true, "your commit is good"];
  }
  if (hasTableChangement && hasDatabase && hasTableName && hasTicket) {
    return [true, ""];
  } else {
    return [
      false,
      'if "table" keyword is present, must be provided: fix/feat(domain): table (database_i.table_name, database_j.table_name)',
    ];
  }
};

const verifyCommit = (parsed, when = "always") => isCommitOk(parsed.header);

module.exports = {
  rules: {
    "table-info-required": verifyCommit,
  },
};
