import os


def lint_description(descripton: str):
    print(f"la description '{descripton}' est valide ! ")


if __name__ == "__main__":
    description = os.environ.get("CI_MERGE_REQUEST_DESCRIPTION")
    if description:
        lint_description(description)
    else:
        print("No description found for Merge Request.")
        exit(1)
